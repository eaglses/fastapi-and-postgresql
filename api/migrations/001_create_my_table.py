steps = [
    [
        # Create the table
        """
        CREATE TABLE recipe (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR NOT NULL,
            uploaded DATE NOT NULL,
            instructions TEXT NOT NULL
        );
        """,
        # drop the table
        """
        DROP TABLE recipe;
        """
    ],
]