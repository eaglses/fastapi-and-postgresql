from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool
from datetime import date


class Error(BaseModel):
    message: str


class RecipeIn(BaseModel):
    name: str
    uploaded: date
    instructions: str


class RecipeOut(BaseModel):
    id: int
    name: str
    uploaded: date
    instructions: str


class RecipeRepository:
    def update(self, recipe_id:int, recipe: RecipeIn) -> Union[Error, RecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE recipe
                        SET name = %s
                        , uploaded = %s
                        , instructions  = %s
                        WHERE id = %s
                        """,
                        [
                            recipe.name,
                            recipe.uploaded,
                            recipe.instructions,
                            recipe_id
                        ]
                    )
                    print(result)
                    return self.recipe_in_to_out(id, recipe) 

        except Exception as e:
            print(e)
            return {
                "message": "update failure"
            }

    def get_all(self) -> Union[Error, List[RecipeOut]]:
        try:
            with pool.connection() as conn:
               with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, uploaded, instructions
                        FROM recipe
                        ORDER BY uploaded;
                        """
                    )
                    result = []
                    for record in db:
                        recipe = RecipeOut(
                            id=record[0],
                            name=record[1],
                            uploaded=record[2],
                            instructions=record[3],
                        )
                        result.append(recipe)
                    return result
        except Exception as e:
            print(e)
            return {
                "message": "could not get all recipes!"
            }

    def create(self, recipe: RecipeIn) -> Union[Error, RecipeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO recipe
                        (name, uploaded, instructions)
                        VALUES
                            (%s, %s, %s)
                            RETURNING id;
                        """,
                        [
                            recipe.name,
                            recipe.uploaded,
                            recipe.instructions
                        ]
                    )
                    id = result.fetchone()[0]
                    # old_data = recipe.dict()
                    # return RecipeOut(id=id, **old_data)
                return self.recipe_in_to_out(id, recipe)
        except Exception as e:
            print(e)
            return {
                "message": "error! likely a bad request"
            }
        
    def recipe_in_to_out(self, id:int, recipe: RecipeIn):
        old_data = recipe.dict()
        return RecipeOut(id=id, **old_data)
 