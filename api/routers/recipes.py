from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.recipes import (
    Error,
    RecipeIn,
    RecipeOut,
    RecipeRepository,
)



router = APIRouter()


@router.post("/recipes", response_model=Union[RecipeOut, Error])
def create_recipe(
    recipe: RecipeIn,
    response: Response,
    repo: RecipeRepository = Depends()
    ):

    response.status_code = 200    
    return repo.create(recipe)


@router.get("/recipes", response_model=Union[List[RecipeOut], Error])
def get_all(
    repo: RecipeRepository = Depends(),

):
    return repo.get_all()


@router.put("/recipes/{recipe_id}", response_model=Union[RecipeOut, Error])
def update_recipe(
    recipe_id: int,
    recipe: RecipeIn,
    repo: RecipeRepository = Depends(),
) -> Union[Error, RecipeOut]:
    return repo.update(recipe_id, recipe)


